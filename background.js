function redirect(requestDetails) {
  console.log("Redirecting: " + requestDetails.url);
  return {
      redirectUrl: (requestDetails.url
        .replace("http://", "https://")
        .replace("i.stack.imgur.com", "simgur.rabb.im")
        .replace("i.imgur.com", "imgur.rabb.im")
        .replace("imgur.com", "imgurp.com")
        .replace(".redd.it", "reddit.rabb.im")
        .replace("i.4cdn.org", "4cdn.rabb.im")
        .replace("e621.net", "e926.net"))
  };
}

browser.webRequest.onBeforeRequest.addListener(
  redirect,
  {urls:[
        "*://*.imgur.com/*",
        "*://i.stack.imgur.com/*",
        "*://i.4cdn.org/*",
        "*://i.redd.it/*",
        "*://external-preview.redd.it/*",
        "*://preview.redd.it/*",
        "*://static1.e621.net/*"
        ]},
  ["blocking"]
);
