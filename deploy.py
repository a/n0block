import subprocess
import json

scp_to_path = "sadstallman:/var/www/n0block/"
public_url = "https://n0block.rabb.im/{}"
fname = "n0block-{}-an+fx.xpi"

with open("updates.json") as f:
    updates_json = json.load(f)

with open("manifest.json") as f:
    manifest_json = json.load(f)

version = manifest_json["version"]
addon_id = manifest_json["browser_specific_settings"]["gecko"]["id"]

update_link = public_url.format(fname.format(version))  # welp
updates_json["addons"][addon_id]["updates"].append({"version": version, "update_link": update_link})

with open("updates.json", "w") as f:
    json.dump(updates_json, f)

subprocess.run("scp updates.json web-ext-artifacts/{} {}".format(fname.format(version), scp_to_path), shell=True, check=True)

