# n0block

WebExtension to rewrite sites blocked in Turkey to proxies hosted by me or others.

## Packaging

`zip -r -FS ../n0block.zip * --exclude .git* README.md LICENSE` + use addons.mozilla.org to get it signed

or

`web-ext sign --api-key apikeyhere --api-secret apisecrethere`

You can get your api key and api secret from https://addons.mozilla.org/en-US/developers/addon/api/key/

## Download

Grab latest xpi from https://n0block.rabb.im/
